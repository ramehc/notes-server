import '@aws-cdk/assert/jest';
import * as cdk from '@aws-cdk/core';
import * as NotesServer from '../lib/notes-server-stack';

test("Notes table has primay key 'author' and sort key 'id'", () => {
  const app = new cdk.App();
  // WHEN
  const stack = new NotesServer.NotesServerStack(app, 'MyTestStack');
  // THEN
  expect(stack).toHaveResource("AWS::DynamoDB::Table", {

      TableName: 'Notes',
      KeySchema: [
        {
          AttributeName: "author",
          KeyType: "HASH"
        },
        {
          AttributeName: "id",
          KeyType: "RANGE"
        }
      ],
      AttributeDefinitions: [
        {
          AttributeName: "author",
          AttributeType: "S"
        },
        {
          AttributeName: "id",
          AttributeType: "S"
        }
      ],
    

  });
});
