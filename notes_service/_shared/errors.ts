import { APIGatewayProxyResult } from 'aws-lambda';

export const defaultHeaders =  {
    "Access-Control-Allow-Headers" : 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,x-requested-with',
    "Access-Control-Allow-Origin": process.env.CORS_ORIGINS ?? '',
}

export class NoteError extends Error{
    statusCode: number;
    headers: any;
    
    constructor(statusCode: number, message: string){
        super(message);
        this.statusCode = statusCode;
        this.headers = defaultHeaders
    }
}

export const errorHandler = (error: any): APIGatewayProxyResult => {
    if(error instanceof NoteError){
        return {
            statusCode: error.statusCode,
            body: error.message,
            headers: error.headers
        }
    }

    if(error instanceof Error){
        return {
            statusCode: 500,
            body: error.message,
            headers: defaultHeaders
        }
    }

    return {
        statusCode: 500,
        body: 'Internal server error',
        headers: defaultHeaders
    }
}