import { APIGatewayEvent, APIGatewayProxyResult } from "aws-lambda";
import { defaultHeaders, errorHandler } from "../_shared/errors";
import { getNotes } from "./get-notes";


export const handler = async (event: APIGatewayEvent): Promise<APIGatewayProxyResult> => {
    try {
        const notes = await getNotes(event, event.queryStringParameters?.id);

        return {
            body: JSON.stringify(notes),
            statusCode: 200,
            headers: defaultHeaders
        };
    } catch (error) {
        console.log(error);
        return errorHandler(error);
    }
}