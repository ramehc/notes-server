import { APIGatewayEvent, APIGatewayProxyResult } from "aws-lambda";
import { defaultHeaders, errorHandler } from "../_shared/errors";
import { deleteNote } from "./delete-note";


export const handler = async (event: APIGatewayEvent): Promise<APIGatewayProxyResult> => {
    try {
        const response = await deleteNote(event);

        return {
            body: JSON.stringify(response),
            statusCode: 200,
            headers: defaultHeaders
        };
    } catch (error) {
        console.log(error);
        return errorHandler(error);
    }
}