import { APIGatewayEvent, APIGatewayProxyResult } from "aws-lambda";
import { defaultHeaders, errorHandler } from "../_shared/errors";
import { getNote } from "./get-note";


export const handler = async (event: APIGatewayEvent): Promise<APIGatewayProxyResult> => {
    try {
        const note = await getNote(event);

        return {
            body: JSON.stringify(note),
            statusCode: 200,
            headers: defaultHeaders
        };
    } catch (error) {
        console.log(error);
        return errorHandler(error);
    }
}